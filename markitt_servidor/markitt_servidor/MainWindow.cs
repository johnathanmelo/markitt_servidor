﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using Proshot.CommandServer;
using Database.Manager;
using Storage.Manager;

namespace markitt_servidor
{
    public partial class MainWindow : Form
    {
        private DatabaseManager db;
        private StorageManager sm;
        private List<ClientManager> clients;
        private BackgroundWorker bwListener;
        private Socket listenerSocket;
        private IPAddress serverIP;
        private int serverPort;

        // Lista dos pares Bag-Local já buscados pelas empilhadeiras
        private List<Tuple<IPAddress, string, string>> ListEmpilhadeirasCodes = new List<Tuple<IPAddress, string, string>>(); // <empilhadeira,bag,local> 

        // Lista de "Bags" já cadastradas que necessitam de ser realocadas
        private List<Tuple<IPAddress,string, string, string, bool, string, string>> ListAllocation = new List<Tuple<IPAddress, string, string, string, bool, string, string>>(); // <empilhadeira,bag,local_anterior,local_proximo,status_em_operacao,status_pega_coloca,tipo_de_operacao>

        // Lista dos pares Produtor-TipoCafé recebidos pela tela de entrada
        private List<Tuple<string, string>> ListEntryDataPack = new List<Tuple<string, string>>(); // <produtor,tipo_café>

        // Lista de "Bags" que serão exportadas
        //private List<Tuple<IPAddress, string, string, string, bool, string>> ListBagVendas = new List<Tuple<IPAddress, string, string, string, bool, string>>(); // <empilhadeira,bag,local_anterior,local_proximo,status_em_operacao,status_pega_coloca>

        public MainWindow()
        {
            InitializeComponent();
            db = new DatabaseManager();
            sm = new StorageManager();
            clients = new List<ClientManager>();
            serverIP = IPAddress.Parse("127.0.0.1");
            serverPort = 8000;
            bwListener = new BackgroundWorker();
            bwListener.WorkerSupportsCancellation = true;
            bwListener.DoWork += new DoWorkEventHandler(StartToListen);
            bwListener.RunWorkerAsync();
            
            // Start timerLogion
            timer.Enabled = true;
            timer.Interval = 15000;
            timer.Tick += new EventHandler(this.timer_Tick);
        }

        

        /* LISTA DOS COMANDOS RECEBIDOS PELO SERVIDOR */

        private void CommandReceived(object sender, CommandEventArgs e)
        {
            if (e.Command.CommandType == CommandType.ClientLoginInform)
            {
                this.SetManagerName(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.QRCodeManual)
            {
                storeQRCodeEmpilhadeira(e.Command.SenderIP, e.Command.MetaData, "Manual");
            }
            else if (e.Command.CommandType == CommandType.QRCodeAuto)
            {
                storeQRCodeEmpilhadeira(e.Command.SenderIP, e.Command.MetaData, "Auto");
            }
            else if (e.Command.CommandType == CommandType.QRCodeToten)
            {
                storeQRCodeToten(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.TotenClientQueueRequest)
            {
                storeTotenClientQueueRequest(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.EntradaClientQueueRequest)
            {
                storeEntradaClientQueueRequest(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.EntryDataPack)
            {
                storeEntryDataPack(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.FinishClientDispatch)
            {
                deleteClientOfEntryDataPack(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.ProvaRequestCoffeeStatusOne)
            {
                storeProvaRequestCoffeeStatusOne(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.ProvaDataPack)
            {
                storeProvaDataPack(e.Command.SenderIP, e.Command.MetaData);
            }
            else if (e.Command.CommandType == CommandType.VendaClientQueueRequest)
            {
                storeVendaClientQueueRequest(e.Command.SenderIP, e.Command.MetaData);
            }

        }        

        #region TotenLogicRegion

        private void storeQRCodeToten(IPAddress iPAddress, string p)
        {
            // Aquisição dos Dados
            string[] message = p.Split(',');
            string bagCode = message[0];
            string clientName = message[1];            

            // Retorna o tipo de café da bag através da ListEntryDataPack
            string coffeeType = FindCoffeeType(clientName);

            // Inserção no Banco
            db.InsertDatabaseBag(bagCode, clientName, coffeeType);

            // Exibição na Tela   
            string msg = "Bag Cadastrada: " + bagCode + "  |  Cliente: " + clientName + "  |  Café: " + coffeeType + "  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), msg);

            // Procura empilhadeira livre
            IPAddress empilhadeira = FindFreeEmpilhadeira();

            if (empilhadeira == null)
                return;

            // Armazena a Bag Recém Cadastrada na Lista de Alocação
            Tuple<IPAddress, string, string, string, bool, string, string> infoBagLocal = sm.getStorageOperation(empilhadeira, bagCode, db.QRCodeLocalToten);
            ListAllocation.Add(infoBagLocal);
        }

        private string FindCoffeeType(string clientName)
        {
            string coffeeType = null;
            foreach (Tuple<string, string> data in ListEntryDataPack)
            {
                if (data.Item1 == clientName)
                {
                    coffeeType = data.Item2;
                    break;
                }
            }

            return coffeeType;
        }

        private void storeTotenClientQueueRequest(IPAddress iPAddress, string p)
        {
            // Exibição na Tela
            string str = "Toten Requisitou Lista de Clientes  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);

            // Busca na ListEntryDataPack gerada na tela de entrada
            string msg = "";
            foreach (Tuple<string,string> data in ListEntryDataPack)
            {
                msg = msg + data.Item1 + ",";
            }
            
            // Envio dos Clientes para Toten
            SendClientQueueRequestToToten(iPAddress, msg);
        }

        #endregion

        #region EntradaLogicRegion

        private void storeEntradaClientQueueRequest(IPAddress iPAddress, string p)
        {
            // Exibição na Tela
            string str = "Tela de Entrada Requisitou Lista de Produtores  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);

            // Busca no Banco            
            List<String> listClients = db.SelectDatabaseAllProducers();

            string msg = "";
            foreach (String cli in listClients)
            {
                msg = msg + cli + ",";
            }

            // Envio dos Clientes para Entrada
            SendClientQueueRequestToEntrada(iPAddress, msg);
        }

        // Armazena na ListEntryDataPack lista dos pares Produtor-TipoCafé recebidos pela tela de entrada
        private void storeEntryDataPack(IPAddress iPAddress, string p)
        {            
            List<string> dataPack = p.Split(',').ToList();
            ListEntryDataPack.Add(new Tuple<string,string>(dataPack[0], dataPack[1]));
            string str = "Tela de Entrada inseriu o produtor " + dataPack[0] + " (" + dataPack[1] + ") na lista de espera para descarregamento  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);
        }

        // Deleta cliente da ListEntryDataPack uma vez que suas bags já foram cadastradas pelo Toten
        private void deleteClientOfEntryDataPack(IPAddress iPAddress, string p)
        {
            ListEntryDataPack.RemoveAll(c => c.Item1 == p);
            string str = "Descarga do produtor " + p + " finalizada  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);
        }

        #endregion

        #region ProvaLogicRegion

        // envia para tela de prova todas bags com status 1 por cliente no formato: "client,type_coffee,number_of_bags|client,type_coffee,number_of_bags"
        private void storeProvaRequestCoffeeStatusOne(IPAddress iPAddress, string p)
        {
            // Exibição na Tela
            string str = "Tela de Prova Requisitou Lista de Bags em Estado de Espera para Prova  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);

            // Busca no Banco: cada string no formato : "client,type_coffee,number_of_bags"
            List<String> listLote = db.SelectDatabaseAllBagsStatusOnePerProducer();

            string msg = "";
            foreach (String lote in listLote)
            {
                msg = msg + lote + "|"; // "client,type_coffee,number_of_bags|client,type_coffee,number_of_bags"
            }

            // Envio das Bags para Tela de Prova
            SendProvaRequestCoffeeStatusOneToProva(iPAddress, msg);
        }

        private void storeProvaDataPack(IPAddress iPAddress, string p)
        {
            // Separa dados
            List<string> resultProva = p.Split(',').ToList();
            string cliente = resultProva[0];
            string firstProvaType = resultProva[1];
            string secondProvaType = resultProva[2];

            // Exibição na Tela
            string str = "Tela de Prova Finalizou Prova do Lote: " + cliente + "  |  Primeira Prova: " + firstProvaType + "  |  Segunda Prova: " + secondProvaType + "  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);

            // Verifica se bag foi APROVADA ou REPROVADA
            int status_prova = 0;

            if (String.Equals(firstProvaType, secondProvaType) == true) // APROVADA
                status_prova = 1;
            else // REPROVADA
                status_prova = 2;            

            // Atualiza os status e a segunda prova das bags
            db.UpdateDatabaseChangeStatusBagStatusProvaSecondProva(cliente, 2, status_prova, secondProvaType); // status_bag = 2 (aguardando definicao da compra)

            // Envia lista de bags com status_bag = 1 atualizada para Tela de Prova
            storeProvaRequestCoffeeStatusOne(iPAddress, null);
        }

        #endregion

        #region VendaLogicRegion

        private void storeVendaClientQueueRequest(IPAddress iPAddress, string p)
        {
            // Exibição na Tela
            string str = "Tela de Venda Requisitou Lista de Compradores  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), str);

            // Busca no Banco            
            List<String> listClients = db.SelectDatabaseAllBuyers();

            string msg = "";
            foreach (String cli in listClients)
            {
                msg = msg + cli + ",";
            }

            // Envio dos Clientes para Toten
            SendClientQueueRequestToVenda(iPAddress, msg);
        }

        #endregion

        #region StorageLogicRegion

        private void storeQRCodeEmpilhadeira(IPAddress iPAddress, string p, string mode)
        {
            // Pegar o par Bag-Local
            Tuple<string, string> pairBagLocal = GetPairBagLocal(iPAddress);

            if (pairBagLocal == null)
                return;

            string desiredBag = pairBagLocal.Item1; 
            string desiredLocal = pairBagLocal.Item2; 

            // Verifica se Código Corresponde a uma Bag ou um Local (0: inválido | 1: bag | 2: local)
            int verificationCode = db.VerifyCode(p);

            // Exibição na Tela
            ScreenExhibitionAboutQRCode(verificationCode, iPAddress, mode, p);

            // Verifica se Bag ou Local estão corretos
            string msg = "";
            if (verificationCode == 0) // Código Inválido
            {
                return;
            }
            else if (verificationCode == 1) // Bag
            {
                if (p == desiredBag) // Bag Correta
                {
                    msg = "BagCorreta";
                }
                else // Bag Errada
                {
                    msg = "BagErrada";
                    SendCodeVerificationResultToEmpilhadeira(iPAddress, msg);
                    return;
                }
            }
            else if (verificationCode == 2) // Local
            {
                if (p == desiredLocal) // Local Correto
                {
                    msg = "LocalCorreto";
                }
                else // Local Errado
                {
                    msg = "LocalErrado";
                    SendCodeVerificationResultToEmpilhadeira(iPAddress, msg);
                    return;
                }
            }

            // Envia Resultado de Verificação do Código para Empilhadeira
            SendCodeVerificationResultToEmpilhadeira(iPAddress, msg);

            // Se Válido, Armazena Código Encontrado
            SaveListEmpilhadeiraCodes(iPAddress, verificationCode, p);

            // Verifica se Operação foi Concluída -> Empilhadeira Pegou a Bag Certa no Lugar Certo            
            bool isConcluded = VerifyConclusionOperation(iPAddress, desiredBag, desiredLocal); // 

            // Envia Resultado de Verificação da Operação
            if (isConcluded == true)
            {
                SendConclusionOperationToEmpilhadeira(iPAddress); // notifica msg de sucesso para empilhadeira                
                ChangeStatusListAllocation(iPAddress,desiredBag,desiredLocal); 
                ScreenExhibitionAboutConcludedOperation(iPAddress, desiredBag, desiredLocal); // exibe log na tela
            }
        }

        private void ChangeStatusListAllocation(IPAddress iPAddress, string desiredBag, string desiredLocal)
        {
            var tuple = ListAllocation.Find(s => s.Item1 == iPAddress);
            
            ListAllocation.RemoveAll(c => c == tuple);

            if (tuple.Item6 == "pega") // empilhadeira pegou a bag, ainda falta colocá-la no local desejado
            {
                tuple = new Tuple<IPAddress, string, string, string, bool, string, string>(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, false, "coloca", tuple.Item7);
                ListAllocation.Add(tuple);
            }
            else if(tuple.Item6 == "coloca") // empilhadeira já colocou a bag no local desejado
            {
                if(tuple.Item7 == "entrada")
                    db.UpdateDatabaseChangeStatusBagQrLocal(desiredBag, desiredLocal, 1); // atualiza informações da bag no banco (status_bag = 1 - aguardando prova)

                if (tuple.Item7 == "venda")
                    db.UpdateDatabaseChangeStatusBagQrLocal(desiredBag, desiredLocal, 6); // atualiza informações da bag no banco (status_bag = 6 - status provisório*)
            }            
        }

        private bool VerifyConclusionOperation(IPAddress iPAddress, string desiredBag, string desiredLocal)
        {
            List<Tuple<IPAddress, string, string>> empilhadeirasCode = ListEmpilhadeirasCodes.FindAll(c => c.Item1 == iPAddress && c.Item2 != "0" && c.Item3 != "0");

            if (empilhadeirasCode.Count == 0)
                return false;

            ListEmpilhadeirasCodes.RemoveAll(c => c.Item1 == iPAddress);    
            return true;
        }

        private void SaveListEmpilhadeiraCodes(IPAddress iPAddress, int verificationCode, string QRCode)
        {
            List<Tuple<IPAddress, string, string>> empilhadeirasCode = ListEmpilhadeirasCodes.FindAll(c => c.Item1 == iPAddress); 

            if (empilhadeirasCode.Count == 0) // Empilhadeira ainda não presente na lista (início da alocação)
            {
                if (verificationCode == 1) // Bag
                {
                    ListEmpilhadeirasCodes.Add(new Tuple<IPAddress, string, string>(iPAddress, QRCode, "0"));
                }
                else if (verificationCode == 2) // Local
                {
                    ListEmpilhadeirasCodes.Add(new Tuple<IPAddress, string, string>(iPAddress, "0", QRCode));
                }
            }        
            else // Empilhadeira já presente na lista (estágio intermediário da alocação)
            {
                if (empilhadeirasCode.Last().Item2 != "0" && empilhadeirasCode.Last().Item3 == "0" && verificationCode == 2) // COM Bag e SEM Local
                {
                    ListEmpilhadeirasCodes.Add(new Tuple<IPAddress, string, string>(empilhadeirasCode.Last().Item1, empilhadeirasCode.Last().Item2, QRCode));
                } 
                else if (empilhadeirasCode.Last().Item2 == "0" && empilhadeirasCode.Last().Item3 != "0" && verificationCode == 1) // SEM Bag e COM Local
                {
                    ListEmpilhadeirasCodes.Add(new Tuple<IPAddress, string, string>(empilhadeirasCode.Last().Item1, QRCode, empilhadeirasCode.Last().Item3));
                }
            }                
        }

        public Tuple<string, string> GetPairBagLocal(IPAddress iPAddress)
        {
            Tuple<string, string> pairBagLocal = null;
            var tuple = ListAllocation.Find(s => s.Item1 == iPAddress);
            //var tupleVenda = ListBagVendas.Find(s => s.Item1 == iPAddress);
            //var tuple = new Tuple<IPAddress, string, string, string, bool, string>(null, null, null, null, false, null);

            if (tuple == null)
                return null;            

            if (tuple.Item6 == "pega")
            {
                pairBagLocal = new Tuple<string, string>(tuple.Item2, tuple.Item3);
            }
            else if(tuple.Item6 == "coloca")
            {
                pairBagLocal = new Tuple<string, string>(tuple.Item2, tuple.Item4);
            }
            return pairBagLocal;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // Preenche ListAllocation com Bags de status 0
            FillListAllocationForEntrada();

            // Preenche ListAllocation com Bags de status 4
            FillListAllocationForVenda();

            // Verifica se existe Bag a ser realocada ou vendida
            if (ListAllocation.Count == 0)
                return;

            foreach(Tuple<IPAddress, string, string, string, bool,string, string> BagLocalToBeAllocated in ListAllocation)
            {
                if(BagLocalToBeAllocated.Item5 == false) // Operação pendente
                {
                    // Envia requisição para empilhadeira
                    string msg = "";
                    if (BagLocalToBeAllocated.Item6 == "pega")
                    {
                        msg = "PEGA BAG " + BagLocalToBeAllocated.Item2 + " no LOCAL " + BagLocalToBeAllocated.Item3;
                    }
                    else if(BagLocalToBeAllocated.Item6 == "coloca")
                    {
                        msg = "COLOCA BAG " + BagLocalToBeAllocated.Item2 + " no LOCAL " + BagLocalToBeAllocated.Item4;
                    }
                    
                    bool msgSent = SendMessageOneEmpilhadeira(BagLocalToBeAllocated.Item1, msg);

                    if (msgSent == true)
                    {
                        // Exibe na tela
                        string p = "Empilhadeira: " + BagLocalToBeAllocated.Item1.ToString() + "  |  " + msg + "  |  " + DateTime.Now;
                        Invoke(new displayOnScreen(showText), p);

                        // Muda status para true: "em operação"
                        var tuple = ListAllocation.Find(s => s.Item1 == BagLocalToBeAllocated.Item1);
                        ListAllocation.RemoveAll(s => s == tuple);
                        tuple = new Tuple<IPAddress, string, string, string, bool,string, string>(BagLocalToBeAllocated.Item1, BagLocalToBeAllocated.Item2, BagLocalToBeAllocated.Item3, BagLocalToBeAllocated.Item4, true, BagLocalToBeAllocated.Item6, BagLocalToBeAllocated.Item7);
                        ListAllocation.Add(tuple);
                        return;
                    }
                }                           
            }            
        }

        private void FillListAllocationForEntrada()
        {
            List<string> listBagStatusZero = db.GetListBagStatusZero();

            foreach(string bagCode in listBagStatusZero)
            {
                // Procura empilhadeira livre
                IPAddress empilhadeira = FindFreeEmpilhadeira();

                if (empilhadeira == null)
                    return;

                // Armazena a Bag Recém Cadastrada na Lista de Alocação
                Tuple<IPAddress,string,string,string,bool,string,string> infoBagLocal = sm.getStorageOperation(empilhadeira, bagCode, db.QRCodeLocalToten);
                ListAllocation.Add(infoBagLocal);
            }
        }

        private void FillListAllocationForVenda()
        {
            List<Tuple<string, string>> listBagStatusQuatro = db.GetListBagStatusCinco();

            foreach (Tuple<string, string> bag in listBagStatusQuatro)
            {
                // Procura empilhadeira livre
                IPAddress empilhadeira = FindFreeEmpilhadeira();

                if (empilhadeira == null)
                    return;

                // Armazena a Bag na Lista de Exportação
                Tuple<IPAddress, string, string, string, bool, string, string> infoBagLocal = sm.BagToExportOperation(empilhadeira, bag.Item1, bag.Item2);
                ListAllocation.Add(infoBagLocal);
            }
        }

        private IPAddress FindFreeEmpilhadeira()
        {
            IPAddress empilhadeira = null;
            foreach (ClientManager mngr in this.clients)
            {
                if (mngr.Connected && mngr.ClientName != null && mngr.ClientName.Equals("Empilhadeira"))
                {
                    empilhadeira = mngr.IP;
                    foreach(Tuple<IPAddress,string,string,string,bool,string,string> c in ListAllocation)
                    { 
                        if(mngr.IP == c.Item1)
                        {
                            empilhadeira = null;
                            break;
                        }
                    }                    
                }
            }
            return empilhadeira;
        }

        #endregion

        #region ScreenExhibitionRegion
        
        delegate void displayOnScreen(string msg);

        private void showText(string msg)
        {
            textBoxLog.AppendText(msg);
            textBoxLog.AppendText(Environment.NewLine);
        }

        private void ScreenExhibitionAboutQRCode(int verificationCode, IPAddress iPAddress, string mode, string p)
        {
            string msg = "";
            if (verificationCode == 0) // código inválido
            {
                msg = "Código Inválido  |  Empilhadeira: " + iPAddress.ToString() + "  |  Modo: " + mode + "  |  " + DateTime.Now;
            }
            else if (verificationCode == 1) // bag
            {
                msg = "Bag: " + p + "  |  Empilhadeira: " + iPAddress.ToString() + "  |  Modo: " + mode + "  |  " + DateTime.Now;
            }
            else if (verificationCode == 2) // Local
            {
                msg = "Local: " + p + "  |  Empilhadeira: " + iPAddress.ToString() + "  |  Modo: " + mode + "  |  " + DateTime.Now;
            }
            Invoke(new displayOnScreen(showText), msg);
        }

        private void ScreenExhibitionAboutConcludedOperation(IPAddress iPAddress, string desiredBag, string desiredLocal)
        {
            string msg = "Alocação Concluída  |  Bag: " + desiredBag + "  |  Local: " + desiredLocal + "  |  Empilhadeira: " + iPAddress.ToString() + "  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), msg);
        }        

        #endregion

        #region CommunicationRegion

        private void StartToListen(object sender, DoWorkEventArgs e)
        {
            this.listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.listenerSocket.Bind(new IPEndPoint(this.serverIP, this.serverPort));
            this.listenerSocket.Listen(200);
            while (true)
            {
                try
                {
                    this.CreateNewClientManager(this.listenerSocket.Accept());
                }
                catch (SocketException)
                {
                    // do nothing
                }
                catch (ObjectDisposedException)
                {
                    // do nothing
                }
            }
        }

        private void CreateNewClientManager(Socket socket)
        {
            ClientManager newClientManager = new ClientManager(socket);
            newClientManager.CommandReceived += new CommandReceivedEventHandler(CommandReceived);
            newClientManager.Disconnected += new DisconnectedEventHandler(ClientDisconnected);
            this.clients.Add(newClientManager);
        }

        private void SendCodeVerificationResultToEmpilhadeira(IPAddress iPAddress, string p)
        {
            Command cmdMessage = new Command(CommandType.CodeVerificationResult, iPAddress, p);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            this.SendCommandToTarget(cmdMessage);
        }

        private void SendConclusionOperationToEmpilhadeira(IPAddress iPAddress)
        {
            Command cmdMessage = new Command(CommandType.OperationConclusion, iPAddress);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            this.SendCommandToTarget(cmdMessage);
        }

        private void SendClientQueueRequestToToten(IPAddress iPAddress, string p)
        {
            Command cmdMessage = new Command(CommandType.TotenClientQueueRequest, iPAddress, p);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            this.SendCommandToTarget(cmdMessage);
        }

        private void SendClientQueueRequestToEntrada(IPAddress iPAddress, string p)
        {
            Command cmdMessage = new Command(CommandType.EntradaClientQueueRequest, iPAddress, p);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            this.SendCommandToTarget(cmdMessage);
        }

        private void SendClientQueueRequestToVenda(IPAddress iPAddress, string p)
        {
            Command cmdMessage = new Command(CommandType.VendaClientQueueRequest, iPAddress, p);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            this.SendCommandToTarget(cmdMessage);
        }

        private void SendProvaRequestCoffeeStatusOneToProva(IPAddress iPAddress, string p)
        {
            Command cmdMessage = new Command(CommandType.ProvaRequestCoffeeStatusOne, iPAddress, p);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            this.SendCommandToTarget(cmdMessage);
        }

        private bool SendMessageOneEmpilhadeira(IPAddress iPAddress, string msg)
        {            
            Command cmdMessage = new Command(CommandType.Message, iPAddress, msg);
            cmdMessage.SenderIP = this.serverIP;
            cmdMessage.SenderName = "Server";
            bool isSent = this.SendCommandToTarget(cmdMessage);
            return isSent;
        }

        private bool SendMessageAllEmpilhadeiras(string msg)
        {
            bool msgSent = false;
            foreach (ClientManager mngr in this.clients)
            {
                if (mngr.Connected && mngr.ClientName.Equals("Empilhadeira"))
                {
                    Command cmdMessage = new Command(CommandType.Message, mngr.IP, msg);
                    cmdMessage.SenderIP = this.serverIP;
                    cmdMessage.SenderName = "Server";
                    this.SendCommandToTarget(cmdMessage);
                    msgSent = true;
                }
            }
            return msgSent;
        }

        private void showClientConnected(IPAddress iPAddress, string p)
        {
            string msg = p + " Conectado(a): " + iPAddress.ToString() + "  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), msg);
        }

        private void showClientDisconnected(IPAddress iPAddress, string p)
        {
            string msg = p + " Desconectado(a): " + iPAddress.ToString() + "  |  " + DateTime.Now;
            Invoke(new displayOnScreen(showText), msg);
        }        

        private bool SendCommandToTarget(Command cmd)
        {
            foreach (ClientManager mngr in this.clients)
            {
                if (mngr.IP.Equals(cmd.Target) && mngr.Connected)
                {
                    mngr.SendCommand(cmd);
                    return true;
                }
            }
            return false;            
        }

        void ClientDisconnected(object sender, ClientEventArgs e)
        {
            this.RemoveClientManager(e.IP);
        }

        private bool RemoveClientManager(IPAddress ip)
        {
            lock (this)
            {
                int index = this.IndexOfClient(ip);
                if (index != -1)
                {
                    string name = this.clients[index].ClientName;
                    this.clients.RemoveAt(index);
                    showClientDisconnected(ip, name);
                    return true;
                }
                return false;
            }
        }

        private int IndexOfClient(IPAddress ip)
        {
            int index = -1;
            foreach (ClientManager cMngr in this.clients)
            {
                index++;
                if (cMngr.IP.Equals(ip))
                    return index;
            }
            return -1;
        }

        private string SetManagerName(IPAddress remoteClientIP, string nameString)
        {
            int index = this.IndexOfClient(remoteClientIP);
            if (index != -1)
            {
                string name = nameString.Split(new char[] { ':' })[1];
                this.clients[index].ClientName = name;
                showClientConnected(remoteClientIP, name);
                return name;
            }
            return "";
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            DisconnectServer();
        }

        public void DisconnectServer()
        {
            if (this.clients != null)
            {
                foreach (ClientManager mngr in this.clients)
                    mngr.Disconnect();

                this.bwListener.CancelAsync();
                this.bwListener.Dispose();
                this.listenerSocket.Close();
                GC.Collect();
            }
        }


        #endregion

        #region TabClientConfigurations

        private void buttonRegisterProducer_Click(object sender, EventArgs e)
        {
            string name = textBoxInsertProducerName.Text;

            if (name.All(c => Char.IsLetter(c) || c == ' ') && !String.IsNullOrWhiteSpace(name))
            {
                db.InsertDatabaseProducer(textBoxInsertProducerName.Text);
                textBoxInsertProducerName.Clear();
                MessageBox.Show("Produtor cadastrado!");
            }
            else
            {
                textBoxInsertProducerName.Clear();
                MessageBox.Show("Entrada inválida", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Warning) ;
            }
        }

        #endregion

        #region TabResultProva

        private void comboBoxResultProva_SelectedIndexChanged(object sender, EventArgs e)
        {
            int status_prova = 0;
            List<Tuple<string, string, string, string>> listBags; // formato: < qr_bag , nome_cliente , tipo_prova_primeira , tipo_prova_segunda >            
            string opt = comboBoxResultProva.Text;
            checkedListBoxResultProva.Items.Clear();

            if (string.Equals(opt, "APROVADOS"))
            {
                status_prova = 1;
            }
            else if (string.Equals(opt, "REPROVADOS"))
            {
                status_prova = 2;
            }

            // Busca todos os clientes do banco
            List<string> listClients = db.SelectDatabaseAllProducers();

            // Busca do banco todas as bags aprovadas/reprovadas (status_prova = 1 ou status_prova = 2)
            listBags = db.SelectDatabaseStatusProvaBags(status_prova);

            // Conta quantas bags existem para cada cliente
            foreach (string cli in listClients)
            {
                var list = listBags.FindAll(c => c.Item2 == cli);

                if (list.Count > 0)
                {
                    string typeCoffeeFirst = list[0].Item3;
                    string typeCoffeeSecond = list[0].Item4;
                    int numberOfBags = list.Count;
                    string str = cli + "  |  Bags: " + numberOfBags + "  |  1ª prova: " + typeCoffeeFirst + "  |  2ª prova: " + typeCoffeeSecond; 
                    checkedListBoxResultProva.Items.Add(str);
                }
            }
        }

        private void checkedListBoxResultProva_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
            {
                for (int ix = 0; ix < checkedListBoxResultProva.Items.Count; ++ix)
                {
                    if (e.Index != ix) checkedListBoxResultProva.SetItemChecked(ix, false);
                }
            }
        }


        #endregion

        private void buttonRegisterBuyer_Click(object sender, EventArgs e)
        {
            string name = textBoxInsertBuyerName.Text;

            if (name.All(c => Char.IsLetter(c) || c == ' ') && !String.IsNullOrWhiteSpace(name))
            {
                db.InsertDatabaseBuyer(textBoxInsertBuyerName.Text);
                textBoxInsertBuyerName.Clear();
                MessageBox.Show("Comprador cadastrado!");
            }
            else
            {
                textBoxInsertBuyerName.Clear();
                MessageBox.Show("Entrada inválida", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }

}
