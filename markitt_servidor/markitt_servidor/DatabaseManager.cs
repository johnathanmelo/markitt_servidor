﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Database.Manager
{
    public class DatabaseManager
    {

        /* INFORMATION ABOUT STATUS OF BAGS:
         * * status_bag: 0 (aguardando alocação), 1 (aguardando prova), 2 (aguardando definicao da compra), 3 (aguardando maquinação), 4 (aguardando venda), 5 (aguardando despacho) 
         * * status_prova: 0 (bag não foi provada), 1 (bag aprovada), 2 (bag rejeitada)
         */

        string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Bernardo\Documents\SourceTree\markitt_servidor\markitt_servidor\markitt_servidor\database.mdf;Integrated Security = True";
        //string connectionString = ConfigurationManager.ConnectionStrings["markitt_servidor.Properties.Settings.databaseConnectionString"].ConnectionString;
        public string QRCodeLocalToten = "0000";
        public string QRCodeLocalBueiro = "0001";

        public List<String> SelectDatabaseAllBuyers()
        {
            List<String> listClients = new List<String>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT Nome FROM Comprador";
                SqlCommand command = new SqlCommand(sql, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string cli = reader.GetString(0).TrimEnd(' ');
                        listClients.Add(cli);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return listClients;
        }

        public List<String> SelectDatabaseAllProducers()
        {
            List<String> listClients = new List<String>();            

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT Nome FROM Produtor";
                SqlCommand command = new SqlCommand(sql, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string cli = reader.GetString(0).TrimEnd(' ');
                        listClients.Add(cli);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return listClients;
        }

        public void InsertDatabaseBuyer(string clientName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "INSERT INTO Comprador(Nome) VALUES(@CompradorNome)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CompradorNome", clientName);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public int SelectDatabaseBuyerID(string clientName)
        {
            int clientID = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT Id FROM Comprador WHERE Nome = @CompradorNome";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CompradorNome", clientName);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        clientID = reader.GetInt32(0);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return clientID;
        }

        public void InsertDatabaseProducer(string clientName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "INSERT INTO Produtor(Nome) VALUES(@ProdutorNome)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@ProdutorNome", clientName);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public int SelectDatabaseProducerID(string clientName)
        {
            int clientID = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT Id FROM Produtor WHERE Nome = @ProdutorNome";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@ProdutorNome", clientName);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        clientID = reader.GetInt32(0);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return clientID;
        }

        // retorna lista de bags aprovadas ou reprovadas no formato: "<qr_bag,nome_Produtor,tipo_primeira_prova,tipo_segunda_prova>"
        public List<Tuple<string,string,string,string>> SelectDatabaseStatusProvaBags(int status_prova) // status_prova = 1 (aprovada) | status_prova = 2 (rejeitada)
        {
            List<Tuple<string, string, string, string>> listBags = new List<Tuple<string, string, string, string>>();

            SqlConnection connection1 = new SqlConnection(connectionString);
            string sql1 = "SELECT Bag.QR, Produtor.Nome, TipoCafe.Tipo FROM Bag INNER JOIN Produtor ON Bag.id_Produtor = Produtor.Id INNER JOIN TipoCafe ON Bag.id_tipo_primeiro = TipoCafe.Id WHERE Bag.status_prova = @StatusProva AND Bag.status_bag = 2";
            SqlCommand command1 = new SqlCommand(sql1, connection1);
            command1.Parameters.AddWithValue("@StatusProva", status_prova);

            SqlConnection connection2 = new SqlConnection(connectionString);
            string sql2 = "SELECT TipoCafe.Tipo FROM TipoCafe INNER JOIN Bag ON Bag.id_tipo_segundo = TipoCafe.Id WHERE Bag.status_prova = @StatusProva AND Bag.status_bag = 2";
            SqlCommand command2 = new SqlCommand(sql2, connection2);
            command2.Parameters.AddWithValue("@StatusProva", status_prova);

            connection1.Open();
            connection2.Open();

            SqlDataReader reader1 = command1.ExecuteReader();
            SqlDataReader reader2 = command2.ExecuteReader();

            if (reader1.HasRows && reader2.HasRows)
            {
                while (reader1.Read() && reader2.Read())
                {
                    string qr_bag = reader1.GetString(0).TrimEnd(' ');
                    string nome_Produtor = reader1.GetString(1).TrimEnd(' ');
                    string tipo_prova_primeira = reader1.GetString(2).TrimEnd(' ');
                    string tipo_prova_segunda = reader2.GetString(0).TrimEnd(' ');
                    Tuple<string, string, string, string> tuple = new Tuple<string, string, string, string>(qr_bag,nome_Produtor, tipo_prova_primeira, tipo_prova_segunda);
                    listBags.Add(tuple);
                }
            }
            reader1.Close();
            reader2.Close();

            connection1.Close();
            connection2.Close();

            return listBags;
        }

        public List<String> SelectDatabaseAllBagsStatusOnePerProducer()
        {
            List<String> listLotes = new List<String>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT Produtor.Nome, TipoCafe.Tipo, COUNT(Bag.ID) FROM Bag INNER JOIN Produtor ON Produtor.Id = Bag.id_Produtor INNER JOIN TipoCafe ON Bag.id_tipo_primeiro = TipoCafe.Id WHERE Bag.status_bag = 1 GROUP BY Produtor.Nome, TipoCafe.Tipo";
                SqlCommand command = new SqlCommand(sql, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string cli = reader.GetString(0).TrimEnd(' ');
                        string type = reader.GetString(1).TrimEnd(' ');
                        int number = reader.GetInt32(2);
                        listLotes.Add(cli + "," + type + "," + number.ToString());
                    }
                }
                reader.Close();
                connection.Close();
            }

            return listLotes;
        }

        public void InsertDatabaseBag(string bagCode, string clientName, string coffeeType)
        {
            // Busca ID do Produtor
            int clientID = SelectDatabaseProducerID(clientName);

            // Busca ID do Tipo de Café
            int coffeeID = SelectDatabaseTypeCoffeeID(coffeeType);

            // Insere Bag no Banco
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "INSERT INTO Bag(QR,id_Produtor,status_bag,id_tipo_primeiro,status_prova) VALUES(@BagCode,@ProdutorID,@StatusBag,@TipoPrimeiroID,@StatusProva)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@BagCode", bagCode);
                command.Parameters.AddWithValue("@ProdutorID", clientID);
                command.Parameters.AddWithValue("@StatusBag", 0);
                command.Parameters.AddWithValue("@TipoPrimeiroID", coffeeID);
                command.Parameters.AddWithValue("@StatusProva", 0);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private int SelectDatabaseTypeCoffeeID(string coffeeType)
        {
            int coffeeID = -1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT Id FROM TipoCafe WHERE Tipo = @CoffeeType";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CoffeeType", coffeeType);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        coffeeID = reader.GetInt32(0);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return coffeeID;
        }

        public int VerifyCode(string QRCode)
        {
            int code = 0; // 0: inválido | 1: bag | 2: local
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Verifica se o código corresponde a uma Bag
                string sql = "SELECT QR FROM Bag WHERE QR = @BagCode";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@BagCode", QRCode);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        code = 1;
                    }
                }
                reader.Close();

                // Verifica se o código corresponde a um Local
                sql = "SELECT QR FROM Local WHERE QR = @LocalCode";
                command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@LocalCode", QRCode);
                reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        code = 2;
                    }
                }
                reader.Close();
                connection.Close();
            }

            return code;
        }

        public string GetRandomLocalToAllocateBag()
        {
            string localQR = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT TOP 5 QR FROM Local WHERE QR != @TotenQRCode AND QR != @BueiroExportCode ORDER BY NEWID()";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@TotenQRCode", QRCodeLocalToten);
                command.Parameters.AddWithValue("@BueiroExportCode", QRCodeLocalBueiro);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        localQR = reader.GetString(0).TrimEnd(' ');
                    }
                }
                reader.Close();
                connection.Close();
            }

            return localQR;
        }

        public List<string> GetListBagStatusZero()
        {
            List<string> listBagStatusZero = new List<string>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT QR FROM Bag WHERE status_bag = @StatusZero";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@StatusZero", 0);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string bagCode = reader.GetString(0).TrimEnd(' ');
                        listBagStatusZero.Add(bagCode);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return listBagStatusZero;
        }

        public List<Tuple<string, string>> GetListBagStatusCinco()
        {
            List<Tuple<string, string>> listBagStatusCinco = new List<Tuple<string, string>>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "SELECT QR, qr_local FROM Bag WHERE status_bag = @StatusCinco";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@StatusCinco", 5);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Tuple<string, string> bag = new Tuple<string, string>(reader.GetString(0).TrimEnd(' '), reader.GetString(1).TrimEnd(' '));
                        listBagStatusCinco.Add(bag);
                    }
                }
                reader.Close();
                connection.Close();
            }

            return listBagStatusCinco;
        }

        public void UpdateDatabaseChangeStatusBagQrLocal(string qr_bag, string qr_local, int status_bag)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "UPDATE Bag SET qr_local = @QRLocal, status_bag = @StatusBag WHERE (QR = @QRBag)";
                SqlCommand command = new SqlCommand(sql, connection);  
                command.Parameters.AddWithValue("@QRLocal", qr_local);
                command.Parameters.AddWithValue("@StatusBag", status_bag);
                command.Parameters.AddWithValue("@QRBag", qr_bag);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void UpdateDatabaseChangeStatusBagStatusProvaSecondProva(string Produtor_nome, int status_bag, int status_prova, string coffee_type)
        {
            // Busca ID do Tipo de Café
            int coffee_id = SelectDatabaseTypeCoffeeID(coffee_type);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sql = "UPDATE Bag SET Bag.status_bag = @StatusBag, Bag.id_tipo_segundo = @TipoSegundoID, status_prova = @StatusProva FROM Bag INNER JOIN Produtor ON Produtor.Id = Bag.id_Produtor WHERE (Produtor.Nome = @ProdutorNome)";
                SqlCommand command = new SqlCommand(sql, connection);                
                command.Parameters.AddWithValue("@StatusBag", status_bag);
                command.Parameters.AddWithValue("@TipoSegundoID", coffee_id);
                command.Parameters.AddWithValue("@StatusProva", status_prova);
                command.Parameters.AddWithValue("@ProdutorNome", Produtor_nome);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        
    }
}
