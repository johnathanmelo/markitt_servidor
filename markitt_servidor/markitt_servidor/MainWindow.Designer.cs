﻿using System.Windows.Forms;

namespace markitt_servidor
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panelMain = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabLogScreen = new System.Windows.Forms.TabPage();
            this.labelLog = new System.Windows.Forms.Label();
            this.tabClientConfiguration = new System.Windows.Forms.TabPage();
            this.labelNameBuyer = new System.Windows.Forms.Label();
            this.labelRegisterBuyer = new System.Windows.Forms.Label();
            this.buttonRegisterBuyer = new System.Windows.Forms.Button();
            this.textBoxInsertBuyerName = new System.Windows.Forms.TextBox();
            this.labelRegisterProducer = new System.Windows.Forms.Label();
            this.buttonRegisterProducer = new System.Windows.Forms.Button();
            this.textBoxInsertProducerName = new System.Windows.Forms.TextBox();
            this.labelNameProducer = new System.Windows.Forms.Label();
            this.tabResultProvaScreen = new System.Windows.Forms.TabPage();
            this.checkedListBoxResultProva = new System.Windows.Forms.CheckedListBox();
            this.labelSelectResultProva = new System.Windows.Forms.Label();
            this.comboBoxResultProva = new System.Windows.Forms.ComboBox();
            this.panelMain.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabLogScreen.SuspendLayout();
            this.tabClientConfiguration.SuspendLayout();
            this.tabResultProvaScreen.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(12, 36);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(1334, 632);
            this.textBoxLog.TabIndex = 2;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.tabControl);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1362, 706);
            this.panelMain.TabIndex = 5;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabLogScreen);
            this.tabControl.Controls.Add(this.tabClientConfiguration);
            this.tabControl.Controls.Add(this.tabResultProvaScreen);
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1360, 700);
            this.tabControl.TabIndex = 4;
            // 
            // tabLogScreen
            // 
            this.tabLogScreen.Controls.Add(this.labelLog);
            this.tabLogScreen.Controls.Add(this.textBoxLog);
            this.tabLogScreen.Location = new System.Drawing.Point(4, 22);
            this.tabLogScreen.Name = "tabLogScreen";
            this.tabLogScreen.Padding = new System.Windows.Forms.Padding(3);
            this.tabLogScreen.Size = new System.Drawing.Size(1352, 674);
            this.tabLogScreen.TabIndex = 0;
            this.tabLogScreen.Text = "Registro de Eventos";
            this.tabLogScreen.UseVisualStyleBackColor = true;
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLog.Location = new System.Drawing.Point(8, 13);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(192, 20);
            this.labelLog.TabIndex = 3;
            this.labelLog.Text = "Log / Registro de Eventos";
            // 
            // tabClientConfiguration
            // 
            this.tabClientConfiguration.Controls.Add(this.labelNameBuyer);
            this.tabClientConfiguration.Controls.Add(this.labelRegisterBuyer);
            this.tabClientConfiguration.Controls.Add(this.buttonRegisterBuyer);
            this.tabClientConfiguration.Controls.Add(this.textBoxInsertBuyerName);
            this.tabClientConfiguration.Controls.Add(this.labelRegisterProducer);
            this.tabClientConfiguration.Controls.Add(this.buttonRegisterProducer);
            this.tabClientConfiguration.Controls.Add(this.textBoxInsertProducerName);
            this.tabClientConfiguration.Controls.Add(this.labelNameProducer);
            this.tabClientConfiguration.Location = new System.Drawing.Point(4, 22);
            this.tabClientConfiguration.Name = "tabClientConfiguration";
            this.tabClientConfiguration.Padding = new System.Windows.Forms.Padding(3);
            this.tabClientConfiguration.Size = new System.Drawing.Size(1352, 674);
            this.tabClientConfiguration.TabIndex = 1;
            this.tabClientConfiguration.Text = "Configurações de Clientes";
            this.tabClientConfiguration.UseVisualStyleBackColor = true;
            // 
            // labelNameBuyer
            // 
            this.labelNameBuyer.AutoSize = true;
            this.labelNameBuyer.Location = new System.Drawing.Point(9, 157);
            this.labelNameBuyer.Name = "labelNameBuyer";
            this.labelNameBuyer.Size = new System.Drawing.Size(35, 13);
            this.labelNameBuyer.TabIndex = 20;
            this.labelNameBuyer.Text = "Nome";
            // 
            // labelRegisterBuyer
            // 
            this.labelRegisterBuyer.AutoSize = true;
            this.labelRegisterBuyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegisterBuyer.Location = new System.Drawing.Point(8, 137);
            this.labelRegisterBuyer.Name = "labelRegisterBuyer";
            this.labelRegisterBuyer.Size = new System.Drawing.Size(202, 20);
            this.labelRegisterBuyer.TabIndex = 19;
            this.labelRegisterBuyer.Text = "Cadastrar Novo Comprador";
            // 
            // buttonRegisterBuyer
            // 
            this.buttonRegisterBuyer.Location = new System.Drawing.Point(12, 214);
            this.buttonRegisterBuyer.Name = "buttonRegisterBuyer";
            this.buttonRegisterBuyer.Size = new System.Drawing.Size(74, 22);
            this.buttonRegisterBuyer.TabIndex = 18;
            this.buttonRegisterBuyer.Text = "Cadastrar";
            this.buttonRegisterBuyer.UseVisualStyleBackColor = true;
            this.buttonRegisterBuyer.Click += new System.EventHandler(this.buttonRegisterBuyer_Click);
            // 
            // textBoxInsertBuyerName
            // 
            this.textBoxInsertBuyerName.Location = new System.Drawing.Point(12, 178);
            this.textBoxInsertBuyerName.Name = "textBoxInsertBuyerName";
            this.textBoxInsertBuyerName.Size = new System.Drawing.Size(364, 20);
            this.textBoxInsertBuyerName.TabIndex = 17;
            // 
            // labelRegisterProducer
            // 
            this.labelRegisterProducer.AutoSize = true;
            this.labelRegisterProducer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegisterProducer.Location = new System.Drawing.Point(8, 13);
            this.labelRegisterProducer.Name = "labelRegisterProducer";
            this.labelRegisterProducer.Size = new System.Drawing.Size(184, 20);
            this.labelRegisterProducer.TabIndex = 16;
            this.labelRegisterProducer.Text = "Cadastrar Novo Produtor";
            // 
            // buttonRegisterProducer
            // 
            this.buttonRegisterProducer.Location = new System.Drawing.Point(12, 90);
            this.buttonRegisterProducer.Name = "buttonRegisterProducer";
            this.buttonRegisterProducer.Size = new System.Drawing.Size(74, 22);
            this.buttonRegisterProducer.TabIndex = 15;
            this.buttonRegisterProducer.Text = "Cadastrar";
            this.buttonRegisterProducer.UseVisualStyleBackColor = true;
            this.buttonRegisterProducer.Click += new System.EventHandler(this.buttonRegisterProducer_Click);
            // 
            // textBoxInsertProducerName
            // 
            this.textBoxInsertProducerName.Location = new System.Drawing.Point(12, 54);
            this.textBoxInsertProducerName.Name = "textBoxInsertProducerName";
            this.textBoxInsertProducerName.Size = new System.Drawing.Size(364, 20);
            this.textBoxInsertProducerName.TabIndex = 14;
            // 
            // labelNameProducer
            // 
            this.labelNameProducer.AutoSize = true;
            this.labelNameProducer.Location = new System.Drawing.Point(9, 37);
            this.labelNameProducer.Name = "labelNameProducer";
            this.labelNameProducer.Size = new System.Drawing.Size(35, 13);
            this.labelNameProducer.TabIndex = 13;
            this.labelNameProducer.Text = "Nome";
            // 
            // tabResultProvaScreen
            // 
            this.tabResultProvaScreen.Controls.Add(this.checkedListBoxResultProva);
            this.tabResultProvaScreen.Controls.Add(this.labelSelectResultProva);
            this.tabResultProvaScreen.Controls.Add(this.comboBoxResultProva);
            this.tabResultProvaScreen.Location = new System.Drawing.Point(4, 22);
            this.tabResultProvaScreen.Name = "tabResultProvaScreen";
            this.tabResultProvaScreen.Padding = new System.Windows.Forms.Padding(3);
            this.tabResultProvaScreen.Size = new System.Drawing.Size(1352, 674);
            this.tabResultProvaScreen.TabIndex = 2;
            this.tabResultProvaScreen.Text = "Resultados da Prova";
            this.tabResultProvaScreen.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxResultProva
            // 
            this.checkedListBoxResultProva.FormattingEnabled = true;
            this.checkedListBoxResultProva.Location = new System.Drawing.Point(16, 72);
            this.checkedListBoxResultProva.Name = "checkedListBoxResultProva";
            this.checkedListBoxResultProva.Size = new System.Drawing.Size(408, 214);
            this.checkedListBoxResultProva.TabIndex = 2;
            this.checkedListBoxResultProva.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxResultProva_ItemCheck);
            // 
            // labelSelectResultProva
            // 
            this.labelSelectResultProva.AutoSize = true;
            this.labelSelectResultProva.Location = new System.Drawing.Point(13, 20);
            this.labelSelectResultProva.Name = "labelSelectResultProva";
            this.labelSelectResultProva.Size = new System.Drawing.Size(136, 13);
            this.labelSelectResultProva.TabIndex = 1;
            this.labelSelectResultProva.Text = "Selecione os tipos de lotes:";
            // 
            // comboBoxResultProva
            // 
            this.comboBoxResultProva.FormattingEnabled = true;
            this.comboBoxResultProva.Items.AddRange(new object[] {
            "APROVADOS",
            "REPROVADOS"});
            this.comboBoxResultProva.Location = new System.Drawing.Point(16, 36);
            this.comboBoxResultProva.Name = "comboBoxResultProva";
            this.comboBoxResultProva.Size = new System.Drawing.Size(141, 21);
            this.comboBoxResultProva.TabIndex = 0;
            this.comboBoxResultProva.SelectedIndexChanged += new System.EventHandler(this.comboBoxResultProva_SelectedIndexChanged);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 706);
            this.Controls.Add(this.panelMain);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Servidor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelMain.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabLogScreen.ResumeLayout(false);
            this.tabLogScreen.PerformLayout();
            this.tabClientConfiguration.ResumeLayout(false);
            this.tabClientConfiguration.PerformLayout();
            this.tabResultProvaScreen.ResumeLayout(false);
            this.tabResultProvaScreen.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel panelMain;
        private TabControl tabControl;
        private TabPage tabLogScreen;
        private TabPage tabClientConfiguration;
        private TabPage tabResultProvaScreen;
        private Label labelRegisterProducer;
        private Button buttonRegisterProducer;
        private TextBox textBoxInsertProducerName;
        private Label labelNameProducer;
        private Label labelLog;
        private ComboBox comboBoxResultProva;
        private Label labelSelectResultProva;
        private CheckedListBox checkedListBoxResultProva;
        private Label labelRegisterBuyer;
        private Button buttonRegisterBuyer;
        private TextBox textBoxInsertBuyerName;
        private Label labelNameBuyer;
    }
}

