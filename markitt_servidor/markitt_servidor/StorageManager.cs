﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.Manager;
using System.Net;

namespace Storage.Manager
{
    public class StorageManager
    {
        private DatabaseManager db = new DatabaseManager();

        // Lógica que busca o melhor local para armazenar a bag - deve ser aprimorada!
        public Tuple<IPAddress,string,string,string,bool,string,string> getStorageOperation(IPAddress empilhadeira, string bagCode, string previousLocal)
        {
            string nextLocal = db.GetRandomLocalToAllocateBag();
            Tuple<IPAddress,string,string,string,bool,string,string> infoBagLocal = new Tuple<IPAddress,string,string,string,bool,string,string>(empilhadeira, bagCode, previousLocal, nextLocal, false, "pega", "entrada");
            return infoBagLocal;
        }

        // Lógica que busca o melhor local para armazenar a bag - deve ser aprimorada!
        public Tuple<IPAddress, string, string, string, bool, string, string> BagToExportOperation(IPAddress empilhadeira, string bagCode, string previousLocal)
        {
            Tuple<IPAddress, string, string, string, bool, string, string> infoBagLocal = new Tuple<IPAddress, string, string, string, bool, string, string>(empilhadeira, bagCode, previousLocal, db.QRCodeLocalBueiro, false, "pega", "venda");
            return infoBagLocal;
        }

    }
}
